package at.itkolleg.springbootstudentenverwaltung.repositories;

import at.itkolleg.springbootstudentenverwaltung.domain.Student;
import at.itkolleg.springbootstudentenverwaltung.exceptions.StudentNichtGefunden;

import java.util.List;

public interface DbZugriffStudenten {

    Student studentSpeichern(Student student);
    List<Student> alleStudenten();
    List<Student> alleStudentenAusDemOrt(String plz);
    Student studentMitId(Long id) throws StudentNichtGefunden;
    void studentLoeschenMitId(Long id);

}
