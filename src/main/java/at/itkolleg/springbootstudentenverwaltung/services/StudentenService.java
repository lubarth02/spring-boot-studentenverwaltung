package at.itkolleg.springbootstudentenverwaltung.services;

import at.itkolleg.springbootstudentenverwaltung.domain.Student;
import at.itkolleg.springbootstudentenverwaltung.exceptions.StudentNichtGefunden;

import java.util.List;

public interface StudentenService {

    List<Student> alleStudenten();

    Student studentEinfuegen(Student student);

    Student studentMitId(Long id) throws StudentNichtGefunden;

    List<Student> alleStudentenMitPlz(String plz);

    void studentLoeschenMitId(Long id);
}
