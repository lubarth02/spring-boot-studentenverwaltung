package at.itkolleg.springbootstudentenverwaltung;

import at.itkolleg.springbootstudentenverwaltung.domain.Student;
import at.itkolleg.springbootstudentenverwaltung.repositories.DbZugriffStudenten;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStudentenverwaltungApplication implements ApplicationRunner {

    @Autowired
    DbZugriffStudenten dbZugriffStudenten;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootStudentenverwaltungApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception { //wird beim starten ausgeführt
        this.dbZugriffStudenten.studentSpeichern(new Student("Lukas Barth","5751"));
        this.dbZugriffStudenten.studentSpeichern(new Student("Günther Hasl","6020"));
        this.dbZugriffStudenten.studentSpeichern(new Student("Max Mustermann","6460"));
    }
}
