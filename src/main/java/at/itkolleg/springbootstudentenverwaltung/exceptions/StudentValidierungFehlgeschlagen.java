package at.itkolleg.springbootstudentenverwaltung.exceptions;

public class StudentValidierungFehlgeschlagen extends Exception {
    public StudentValidierungFehlgeschlagen(String message) {
        super(message);
    }
}
