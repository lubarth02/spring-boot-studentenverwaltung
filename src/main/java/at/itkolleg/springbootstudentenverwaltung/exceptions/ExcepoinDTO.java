package at.itkolleg.springbootstudentenverwaltung.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ExcepoinDTO {

    private String code;
    private String message;
}
