package at.itkolleg.springbootstudentenverwaltung.exceptions;

public class StudentNichtGefunden extends Exception {

    public StudentNichtGefunden(String message) {
        super(message);
    }
}
